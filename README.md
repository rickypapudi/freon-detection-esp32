# ESP32 Gas Leak Monitoring System

This project involves an ESP32 microcontroller interfaced with an MQ135 gas sensor for monitoring gas leak. The system is integrated with the Blynk platform for remote monitoring and data logging.

## Hardware Components

- ESP32 Development Board
- MQ135 Gas Sensor
- Wi-Fi Module
- Resistors
- Connecting Wires

## Dependencies

- [Blynk Library](https://github.com/blynkkk/blynk-library)
- [Arduino Core for ESP32](https://github.com/espressif/arduino-esp32)
- [MQ135 Library](https://github.com/GeorgK/MQ135)

## Installation

1. Clone or download the necessary libraries and add them to your Arduino IDE.
2. Install the Arduino Core for ESP32.
3. Set up the Blynk project with the appropriate widgets for data visualization.

## Configuration

- Modify the following parameters in the code according to your setup:
  - `BLYNK_TEMPLATE_ID`: Blynk Template ID
  - `BLYNK_TEMPLATE_NAME`: Blynk Template Name
  - `BLYNK_AUTH_TOKEN`: Blynk Authentication Token
  - `ssid`: Wi-Fi SSID
  - `pass`: Wi-Fi Password

## Usage

1. Upload the code to your ESP32 board.
2. Power up the board and monitor the serial output to ensure proper initialization.
3. Connect to the Blynk app to visualize real-time data and receive alerts.

## Functionality

- The system periodically measures the air quality using the MQ135 sensor and sends the data to the Blynk app for visualization.
- If the air quality exceeds a certain threshold (100 PPM in this case), an alert is sent to the Blynk app.
- Additionally, local monitoring via the serial interface is provided for debugging purposes.

## Notes

- Ensure proper calibration of the MQ135 sensor for accurate air quality measurements.
- Customize the system behavior and thresholds according to your specific requirements.

## Author

- [chronomustard](https://github.com/chronomustard)

