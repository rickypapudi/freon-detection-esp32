#define BLYNK_TEMPLATE_ID "TMPL6hsLVcNJ7"
#define BLYNK_TEMPLATE_NAME "Quickstart Template"
#define BLYNK_AUTH_TOKEN "pyRfqJGHxB5rwpasbPbPnoUHWfKDQvXB"

#include <Arduino.h>
#include <WiFi.h>
#include <Wire.h>
#include <WiFiClient.h>
#include <Blynk.h>
#include <BlynkSimpleEsp32.h>
#include <MQ135.h>

#define MQ_Pin 35
#define BLYNK_PRINT Serial
//#define RL1 35 // Relay Sensor Pin

char auth[] = BLYNK_AUTH_TOKEN;
const char* ssid = "Ciki"; // CHANGE THIS
const char* pass = "kiky123321"; // CHANGE THIS
unsigned long prevMillis = 0;
const long interval = 1000;

float avg_ppm = 0.0;

float ppm_0 = 0.0; //state 1
float ppm_1 = 0.0; //state 2
float ppm_2 = 0.0; //state 3
float ppm_3 = 0.0; //state 4

float temperature = 21.0; // Assume current temperature. Recommended to measure with DHT22
float humidity = 25.0; // Assume current humidity. Recommended to measure with DHT22

float ppm = 0;

SimpleTimer timer;
WiFiServer server(80);

// BLYNK_WRITE(V1) //Blynk Relay Connection to MQ Sensor
// {
//   int pinValue1 = param.asInt();
//   if (pinValue1 == 1){
//     digitalWrite(RL1, LOW);
//   }
//   else {
//     digitalWrite(RL1, HIGH);
//   }
// }

void getSendData(){ //Send data function
  MQ135 gasSensor = MQ135(MQ_Pin);
  float ppm = analogRead(MQ_Pin)*0.1; //Calibration
  Blynk.virtualWrite(V0, ppm); //Write to Blynk for data logging
  if (avg_ppm - ppm >= 15){
    Serial.print("\nFREON LEAKAGE ALERT\n"); //Write alert to local monitor for debugging
    Blynk.logEvent("gas_leak_detected"); //Write alert to Blynk for data logging
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(MQ_Pin, INPUT);
  // pinMode(RL1, OUTPUT); //Relay Sensor Output Pinmode
  // digitalWrite(RL1, LOW); //Initialize sensor off when starting
  Blynk.begin(auth, ssid, pass); //Initialize blynk via SSID
  timer.setInterval(2500L, getSendData); //Init timer interval for data logging to Blynk
  delay(2000); //Delay for processing

  Serial.print("Connecting to ");
  Serial.print(ssid);
  WiFi.begin(ssid, pass); //Begin connection via WiFi SSID
  while (WiFi.status() != WL_CONNECTED){ //Connection error handling
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi is connected");
  server.begin();
  Serial.println("Server started");
  Serial.println(WiFi.localIP()); //Print local IP address to check connection
  Serial.println();
  delay(2000); //Delay for processing
}

void loop() {
  timer.run(); //Run timer
  Blynk.run(); //Run blynk
  MQ135 gasSensor = MQ135(MQ_Pin);
  float ppm = analogRead(MQ_Pin)*0.1; //Get air quality raw reading (For debugging purposes)

  unsigned long currentMillis = millis(); //Additional loop timer for local monitor data logging and debugging
  if (currentMillis - prevMillis >= interval) {

    ppm_3 = ppm_2;
    ppm_2 = ppm_1;
    ppm_1 = ppm_0;
    ppm_0 = ppm;

    avg_ppm = (ppm_0 + ppm_1 + ppm_2 + ppm_3)/4;

    Serial.print("PPM: ");
    Serial.print(ppm);
    Serial.print(" PPM");
    Serial.println();
    
    prevMillis = millis();
  }
  //if (digitalRead(RL1)==HIGH){
    
  //}
}
